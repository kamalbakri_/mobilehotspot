package com.simp.hostpotmobile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.simp.hostpotmobile.R;
import com.simp.hostpotmobile.database.entities.HotspotEntities;

import java.util.ArrayList;
import java.util.List;

public class RecyclerHotspotAdapter extends RecyclerView.Adapter<RecyclerHotspotAdapter.MyViewHolder> {
    private Context mContext;
    private List<HotspotEntities> hotspots;
    private List<HotspotEntities> hotspotsFull;
    private HotspotListener hotspotListener;
    private int row_index = 1;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvHotspotId, tvDate, tvSatellite, tvArea, tvEstate, tvBlok, tvDistance, tvRing, tvDirection, tvRing2Outside;

        public MyViewHolder(View view) {
            super(view);
            tvHotspotId = view.findViewById(R.id.tv_hotspotid);
            tvDate = view.findViewById(R.id.tv_date);
            tvSatellite = view.findViewById(R.id.tv_satellite);
            tvArea = view.findViewById(R.id.tv_area);
            tvEstate = view.findViewById(R.id.tv_estate);
            tvBlok = view.findViewById(R.id.tv_blok);
            tvDistance = view.findViewById(R.id.tv_distance);
            tvRing = view.findViewById(R.id.tv_ring);
            tvDirection = view.findViewById(R.id.tv_direction);
            tvRing2Outside = view.findViewById(R.id.tv_ring2outside);
        }
    }

    public RecyclerHotspotAdapter(Context mContext, HotspotListener hotspotListener) {
        this.mContext = mContext;
        this.hotspotListener = hotspotListener;
    }

    public void setData(List<HotspotEntities> hotspots) {
        this.hotspots = hotspots;
        hotspotsFull = new ArrayList<>(hotspots);
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_summary, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        HotspotEntities hotspot = hotspots.get(position);

        holder.tvHotspotId.setText(hotspot.getHotspotID());
        holder.tvDate.setText(hotspot.getDatetime().replaceAll("T", " "));
        holder.tvSatellite.setText(hotspot.getSatellite());
        holder.tvArea.setText(hotspot.getArea());
        holder.tvEstate.setText(hotspot.getBusArea());
        holder.tvBlok.setText(hotspot.getBlock());
        holder.tvDistance.setText(String.valueOf(hotspot.getDistanceKm()));
        holder.tvRing.setText(hotspot.getRing());
        holder.tvDirection.setText(hotspot.getDirection());
        holder.tvRing2Outside.setText("");
    }

    @Override
    public int getItemCount() {
        if (hotspots == null) {
            hotspots = new ArrayList<>();
        }
        return hotspots.size();
    }


    public Filter getLogbookFilter() {
        return logbookFilter;
    }

    private Filter logbookFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ArrayList<HotspotEntities> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(hotspotsFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (HotspotEntities item : hotspotsFull) {
                    if (item.getHotspotID().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            hotspots.clear();
            hotspots.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };


    public interface HotspotListener {
        void onHotspotItemClick(HotspotEntities hotspotEntities);
    }


}