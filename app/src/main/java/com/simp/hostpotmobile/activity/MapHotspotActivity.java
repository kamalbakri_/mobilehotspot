package com.simp.hostpotmobile.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.simp.hostpotmobile.R;
import com.simp.hostpotmobile.utils.Permissions;
import com.simp.hostpotmobile.utils.ViewAnimation;

public class MapHotspotActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    boolean isRotate = false;
    String mapType = "default";

    FloatingActionButton fabMenu, fabTable, fabFilter, fabBasemap, fabLayer, fabLegenda,
            fabZoomIn, fabZoomOut, fabCurrentPosition;

    GoogleMap gMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_hotspot);
        hideStatusBar();
        initView();

        // Get the SupportMapFragment and request notification when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    public void initView() {
        fabMenu = findViewById(R.id.fab_menu);
        fabTable = findViewById(R.id.fab_table);
        fabFilter = findViewById(R.id.fab_filter);
        fabBasemap = findViewById(R.id.fab_basemap);
        fabLayer = findViewById(R.id.fab_layer);
        fabLegenda = findViewById(R.id.fab_legenda);
        fabZoomIn = findViewById(R.id.fab_zoomin);
        fabZoomOut = findViewById(R.id.fab_zoomout);
        fabCurrentPosition = findViewById(R.id.fab_current);


        fabMenu.setOnClickListener(this);
        fabTable.setOnClickListener(this);
        fabFilter.setOnClickListener(this);
        fabBasemap.setOnClickListener(this);
        fabLayer.setOnClickListener(this);
        fabLegenda.setOnClickListener(this);
        fabCurrentPosition.setOnClickListener(this);
        fabZoomIn.setOnClickListener(this);
        fabZoomOut.setOnClickListener(this);

        ViewAnimation.init(fabTable);
        ViewAnimation.init(fabFilter);
        ViewAnimation.init(fabBasemap);
        ViewAnimation.init(fabLayer);
        ViewAnimation.init(fabLegenda);
    }


    public void hideStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_menu:
                showMenu(view);
                break;
            case R.id.fab_legenda:
                showLegend();
                break;
            case R.id.fab_layer:
                break;
            case R.id.fab_filter:
                showFilter();
                break;
            case R.id.fab_basemap:
                showBasemap();
                break;
            case R.id.fab_table:
                Intent intent = new Intent(MapHotspotActivity.this, SummaryHotspotActivity.class);
                startActivity(intent);
                break;
            case R.id.fab_current:
                if (!Permissions.checkFineLocation(this)) {
                    Permissions.requestFineLocation(this, MY_PERMISSIONS_REQUEST_LOCATION);
                } else {
                    getCurrentPosition();
                }
                break;
            case R.id.fab_zoomin:
                gMap.animateCamera(CameraUpdateFactory.zoomIn());
                break;
            case R.id.fab_zoomout:
                gMap.animateCamera(CameraUpdateFactory.zoomOut());
                break;

            default:
                break;
        }
    }

    public void showMenu(View view) {
        isRotate = ViewAnimation.rotateFab(view, !isRotate, getApplicationContext());
        if (isRotate) {
            ViewAnimation.showIn(fabTable);
            ViewAnimation.showIn(fabFilter);
            ViewAnimation.showIn(fabBasemap);
            ViewAnimation.showIn(fabLayer);
            ViewAnimation.showIn(fabLegenda);
        } else {
            ViewAnimation.showOut(fabTable);
            ViewAnimation.showOut(fabFilter);
            ViewAnimation.showOut(fabBasemap);
            ViewAnimation.showOut(fabLayer);
            ViewAnimation.showOut(fabLegenda);
        }
    }

    public void showLegend() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialoglayout = getLayoutInflater().inflate(R.layout.dialog_legend, null);
        dialogBuilder.setView(dialoglayout);
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
    }

    public void showBasemap() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialoglayout = getLayoutInflater().inflate(R.layout.dialog_basemap, null);
        dialogBuilder.setView(dialoglayout);

        RadioGroup rgBasemap = dialoglayout.findViewById(R.id.rg_basemap);

        if (mapType.equals("default")) {
            rgBasemap.check(R.id.rb_map_default);
        } else if (mapType.equals("satellite")) {
            rgBasemap.check(R.id.rb_map_satellite);
        }

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();

        rgBasemap.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.rb_map_default:
                        if (!mapType.equals("default")) {
                            mapType = "default";
                            gMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                        }
                        alertDialog.dismiss();
                        break;
                    case R.id.rb_map_satellite:
                        if (!mapType.equals("satellite")) {
                            mapType = "satellite";
                            gMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                        }
                        alertDialog.dismiss();
                        break;
                    default:
                        break;
                }
            }
        });
    }


    public void showFilter() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialoglayout = getLayoutInflater().inflate(R.layout.dialog_filter, null);
        dialogBuilder.setView(dialoglayout);
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
//        setUpClusterManager();
        LatLng indonesia = new LatLng(-6.2082572, 106.8226707);
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(indonesia, 10));
    }

    public void getCurrentPosition() {
        // GET CURRENT LOCATION
        FusedLocationProviderClient mFusedLocation = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocation.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    // Do it all with location
                    // Display in Toast
                    Toast.makeText(MapHotspotActivity.this,
                            "Lat : " + location.getLatitude() + " Long : " + location.getLongitude(),
                            Toast.LENGTH_LONG).show();
                    gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 16));
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getCurrentPosition();
                }
            }
        }
    }

}
