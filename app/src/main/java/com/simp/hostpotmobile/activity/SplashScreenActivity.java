package com.simp.hostpotmobile.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import com.simp.hostpotmobile.R;
import com.simp.hostpotmobile.database.entities.MenaraApiEntities;
import com.simp.hostpotmobile.database.repository.DbHotspotRepository;
import com.simp.hostpotmobile.retrofit.ApiClient;
import com.simp.hostpotmobile.retrofit.ApiInterface;
import com.simp.hostpotmobile.retrofit.response.MenaraApi;
import com.simp.hostpotmobile.retrofit.response.MenaraApiResponse;
import com.simp.hostpotmobile.utils.Constant;
import com.simp.hostpotmobile.workmanager.HotspotWorker;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreenActivity extends AppCompatActivity {

    ApiInterface apiInterface;
    DbHotspotRepository dbHotspotRepository;
    Integer countMenaraApi = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        runningSplashScreen();
    }

    public void runningSplashScreen() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isFinishing()) {

                    if (isNetworkConnected()) {
                        getDataExisting();
                    } else {
                        Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                        nextActivity();
                    }


                }
            }
        }, 1000);
    }

    public void nextActivity() {
        Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected()) {
            return isConnected();
        } else {
            return false;
        }
    }

    public boolean isConnected() {
        final String command = "ping -c 1 webapp.indoagri.co.id";
        try {
            return Runtime.getRuntime().exec(command).waitFor() == 0;
        } catch (Exception e) {
            return false;
        }
    }

    public void getDataExisting() {
        apiInterface = ApiClient.getClient(Constant.BASE_URL).create(ApiInterface.class);
        dbHotspotRepository = new DbHotspotRepository(getApplicationContext());
        getCountMenaraApi();

        getDataMenaraApi().enqueue(new Callback<MenaraApiResponse>() {
            @Override
            public void onResponse(Call<MenaraApiResponse> call, Response<MenaraApiResponse> response) {
                if (response.code() == 200) {
                    MenaraApiResponse menaraApiResponse = fetchMenaraApi(response);
                    if (menaraApiResponse.getCountData() > countMenaraApi) {
                        new InsertMenaraApi().execute(menaraApiResponse.getHotspot());
                    } else {
                        nextActivity();
                    }
                } else {
                    nextActivity();
                    Toast.makeText(getApplicationContext(), "Invalid load data menara api", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MenaraApiResponse> call, Throwable t) {

            }
        });
    }

    private Call<MenaraApiResponse> getDataMenaraApi() {
        return apiInterface.getDataMenaraApi(0, 1000);
    }

    private MenaraApiResponse fetchMenaraApi(Response<MenaraApiResponse> response) {
        MenaraApiResponse menaraApi = response.body();
        return menaraApi;
    }

    public class InsertMenaraApi extends AsyncTask<List<MenaraApi>, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (countMenaraApi > 0)
                dbHotspotRepository.deleteAllMenaraApi();
        }

        @Override
        protected Void doInBackground(List<MenaraApi>... lists) {

            for (MenaraApi menaraApi : lists[0]) {
                MenaraApiEntities menaraApiEntities = new MenaraApiEntities();
                menaraApiEntities.setObjectid(menaraApi.getObjectid());
                menaraApiEntities.setId(menaraApi.getId());
                menaraApiEntities.setArea(menaraApi.getArea());
                menaraApiEntities.setBusArea(menaraApi.getBusArea());
                menaraApiEntities.setNo(menaraApi.getNo());
                menaraApiEntities.setInformasi(menaraApi.getInformasi());
                menaraApiEntities.setDivisi(menaraApi.getDivisi());
                menaraApiEntities.setKodeBlok(menaraApi.getKodeBlok());

                String stringpoint = menaraApi.getTheGeom();
                if (stringpoint.contains("POINT")) {
                    stringpoint = stringpoint.replace("POINT (", "").replace(")", "");
                    String[] longlat = stringpoint.split(" ");
                    menaraApiEntities.setLongitude(Double.valueOf(longlat[0]));
                    menaraApiEntities.setLatitude(Double.valueOf(longlat[1]));
                }
                dbHotspotRepository.insertMenaraApi(menaraApiEntities);
            }
//            startWorkManager();
            nextActivity();
            return null;
        }
    }

    public void getCountMenaraApi() {
        dbHotspotRepository.countAllMenaraApi().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                countMenaraApi = integer;
            }
        });
    }



}
