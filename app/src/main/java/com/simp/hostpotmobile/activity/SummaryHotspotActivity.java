package com.simp.hostpotmobile.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.simp.hostpotmobile.R;
import com.simp.hostpotmobile.adapter.RecyclerHotspotAdapter;
import com.simp.hostpotmobile.database.entities.HotspotEntities;
import com.simp.hostpotmobile.database.repository.DbHotspotRepository;

import java.util.ArrayList;
import java.util.List;

public class SummaryHotspotActivity extends AppCompatActivity implements RecyclerHotspotAdapter.HotspotListener {

    RecyclerView rvSummary;
    List<HotspotEntities> hotspotEntities;
    RecyclerHotspotAdapter recyclerHotspotAdapter;
    DbHotspotRepository dbHotspotRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary_hotspot);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Summary Hotspot");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        dbHotspotRepository = new DbHotspotRepository(getApplicationContext());

        rvSummary = findViewById(R.id.rv_summary);
        recyclerHotspotAdapter = new RecyclerHotspotAdapter(getApplicationContext(), this);
        LinearLayoutManager layoutManagerFeed
                = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
        rvSummary.setLayoutManager(layoutManagerFeed);
        rvSummary.setItemAnimator(new DefaultItemAnimator());
        rvSummary.setAdapter(recyclerHotspotAdapter);

        selectHotspot();
    }

    public void selectHotspot() {
        dbHotspotRepository.selectAllHotspot().observe(this, new Observer<List<HotspotEntities>>() {
            @Override
            public void onChanged(List<HotspotEntities> hotspotEntities) {
                recyclerHotspotAdapter.setData(hotspotEntities);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onHotspotItemClick(HotspotEntities hotspotEntities) {

    }
}
