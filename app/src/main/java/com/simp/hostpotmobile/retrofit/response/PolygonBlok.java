package com.simp.hostpotmobile.retrofit.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PolygonBlok {
    @SerializedName("objectid")
    @Expose
    private Integer objectiD1;
    @SerializedName("bloK_ID")
    @Expose
    private String bloKID;
    @SerializedName("kodE_KEBUN")
    @Expose
    private String kodEKEBUN;
    @SerializedName("kodE_BLOK")
    @Expose
    private String kodEBLOK;
    @SerializedName("busarea")
    @Expose
    private String busarea;
    @SerializedName("kodearea")
    @Expose
    private String kodearea;
    @SerializedName("keterangan")
    @Expose
    private String keterangan;
    @SerializedName("kodedivisi")
    @Expose
    private String kodedivisi;
    @SerializedName("kodE_PT")
    @Expose
    private String kodEPT;
    @SerializedName("division")
    @Expose
    private String division;
    @SerializedName("tH_TANAM")
    @Expose
    private String tHTANAM;
    @SerializedName("the_geom")
    @Expose
    private String theGeom;

    public Integer getObjectiD1() {
        return objectiD1;
    }

    public void setObjectiD1(Integer objectiD1) {
        this.objectiD1 = objectiD1;
    }

    public String getBloKID() {
        return bloKID;
    }

    public void setBloKID(String bloKID) {
        this.bloKID = bloKID;
    }

    public String getKodEKEBUN() {
        return kodEKEBUN;
    }

    public void setKodEKEBUN(String kodEKEBUN) {
        this.kodEKEBUN = kodEKEBUN;
    }

    public String getKodEBLOK() {
        return kodEBLOK;
    }

    public void setKodEBLOK(String kodEBLOK) {
        this.kodEBLOK = kodEBLOK;
    }

    public String getBusarea() {
        return busarea;
    }

    public void setBusarea(String busarea) {
        this.busarea = busarea;
    }

    public String getKodearea() {
        return kodearea;
    }

    public void setKodearea(String kodearea) {
        this.kodearea = kodearea;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getKodedivisi() {
        return kodedivisi;
    }

    public void setKodedivisi(String kodedivisi) {
        this.kodedivisi = kodedivisi;
    }

    public String getKodEPT() {
        return kodEPT;
    }

    public void setKodEPT(String kodEPT) {
        this.kodEPT = kodEPT;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getTHTANAM() {
        return tHTANAM;
    }

    public void setTHTANAM(String tHTANAM) {
        this.tHTANAM = tHTANAM;
    }

    public String getTheGeom() {
        return theGeom;
    }

    public void setTheGeom(String theGeom) {
        this.theGeom = theGeom;
    }

}
