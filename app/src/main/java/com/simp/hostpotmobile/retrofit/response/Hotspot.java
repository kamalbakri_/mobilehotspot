package com.simp.hostpotmobile.retrofit.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Hotspot {


    @SerializedName("objectid")
    @Expose
    private Integer objectid;
    @SerializedName("no")
    @Expose
    private Integer no;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("datetime")
    @Expose
    private String datetime;
    @SerializedName("satellite")
    @Expose
    private String satellite;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("distance_km")
    @Expose
    private Double distanceKm;
    @SerializedName("sudut")
    @Expose
    private Double sudut;
    @SerializedName("busArea")
    @Expose
    private String busArea;
    @SerializedName("direction")
    @Expose
    private String direction;
    @SerializedName("ring")
    @Expose
    private String ring;
    @SerializedName("uploadBy")
    @Expose
    private String uploadBy;
    @SerializedName("uploadDate")
    @Expose
    private String uploadDate;
    @SerializedName("konfirmasi")
    @Expose
    private String konfirmasi;
    @SerializedName("konfirmDate")
    @Expose
    private String konfirmDate;
    @SerializedName("block")
    @Expose
    private String block;
    @SerializedName("ket")
    @Expose
    private String ket;
    @SerializedName("kodeArea")
    @Expose
    private String kodeArea;
    @SerializedName("pt")
    @Expose
    private String pt;
    @SerializedName("grup")
    @Expose
    private String grup;
    @SerializedName("hotspotID")
    @Expose
    private String hotspotID;

    public Integer getObjectid() {
        return objectid;
    }

    public void setObjectid(Integer objectid) {
        this.objectid = objectid;
    }

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getSatellite() {
        return satellite;
    }

    public void setSatellite(String satellite) {
        this.satellite = satellite;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Double getDistanceKm() {
        return distanceKm;
    }

    public void setDistanceKm(Double distanceKm) {
        this.distanceKm = distanceKm;
    }

    public Double getSudut() {
        return sudut;
    }

    public void setSudut(Double sudut) {
        this.sudut = sudut;
    }

    public String getBusArea() {
        return busArea;
    }

    public void setBusArea(String busArea) {
        this.busArea = busArea;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getRing() {
        return ring;
    }

    public void setRing(String ring) {
        this.ring = ring;
    }

    public String getUploadBy() {
        return uploadBy;
    }

    public void setUploadBy(String uploadBy) {
        this.uploadBy = uploadBy;
    }

    public String getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getKonfirmasi() {
        return konfirmasi;
    }

    public void setKonfirmasi(String konfirmasi) {
        this.konfirmasi = konfirmasi;
    }

    public String getKonfirmDate() {
        return konfirmDate;
    }

    public void setKonfirmDate(String konfirmDate) {
        this.konfirmDate = konfirmDate;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }

    public String getKodeArea() {
        return kodeArea;
    }

    public void setKodeArea(String kodeArea) {
        this.kodeArea = kodeArea;
    }

    public String getPt() {
        return pt;
    }

    public void setPt(String pt) {
        this.pt = pt;
    }

    public String getGrup() {
        return grup;
    }

    public void setGrup(String grup) {
        this.grup = grup;
    }

    public String getHotspotID() {
        return hotspotID;
    }

    public void setHotspotID(String hotspotID) {
        this.hotspotID = hotspotID;
    }
}
