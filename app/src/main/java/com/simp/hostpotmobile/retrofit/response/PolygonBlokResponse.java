package com.simp.hostpotmobile.retrofit.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PolygonBlokResponse {

    @SerializedName("polygonBlok")
    @Expose
    private List<PolygonBlok> polygonBlok = null;

    public List<PolygonBlok> getPolygonBlok() {
        return polygonBlok;
    }

    public void setPolygonBlok(List<PolygonBlok> polygonBlok) {
        this.polygonBlok = polygonBlok;
    }
}
