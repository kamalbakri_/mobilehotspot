package com.simp.hostpotmobile.retrofit.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MenaraApi {

    @SerializedName("objectid")
    @Expose
    private Integer objectid;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("busArea")
    @Expose
    private String busArea;
    @SerializedName("no")
    @Expose
    private Integer no;
    @SerializedName("informasi")
    @Expose
    private String informasi;
    @SerializedName("divisi")
    @Expose
    private String divisi;
    @SerializedName("kode_Blok")
    @Expose
    private String kodeBlok;
    @SerializedName("the_Geom")
    @Expose
    private String theGeom;

    public Integer getObjectid() {
        return objectid;
    }

    public void setObjectid(Integer objectid) {
        this.objectid = objectid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getBusArea() {
        return busArea;
    }

    public void setBusArea(String busArea) {
        this.busArea = busArea;
    }

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public String getInformasi() {
        return informasi;
    }

    public void setInformasi(String informasi) {
        this.informasi = informasi;
    }

    public String getDivisi() {
        return divisi;
    }

    public void setDivisi(String divisi) {
        this.divisi = divisi;
    }

    public String getKodeBlok() {
        return kodeBlok;
    }

    public void setKodeBlok(String kodeBlok) {
        this.kodeBlok = kodeBlok;
    }

    public String getTheGeom() {
        return theGeom;
    }

    public void setTheGeom(String theGeom) {
        this.theGeom = theGeom;
    }
}
