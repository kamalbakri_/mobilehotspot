package com.simp.hostpotmobile.retrofit.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HotspotResponse {

    @SerializedName("hotspot")
    @Expose
    private List<Hotspot> hotspot = null;
    @SerializedName("countPage")
    @Expose
    private Integer countPage;
    @SerializedName("countData")
    @Expose
    private Integer countData;

    public List<Hotspot> getHotspot() {
        return hotspot;
    }

    public void setHotspot(List<Hotspot> hotspot) {
        this.hotspot = hotspot;
    }

    public Integer getCountPage() {
        return countPage;
    }

    public void setCountPage(Integer countPage) {
        this.countPage = countPage;
    }

    public Integer getCountData() {
        return countData;
    }

    public void setCountData(Integer countData) {
        this.countData = countData;
    }
}
