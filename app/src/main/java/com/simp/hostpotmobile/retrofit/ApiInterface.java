package com.simp.hostpotmobile.retrofit;

import com.simp.hostpotmobile.retrofit.response.HotspotResponse;
import com.simp.hostpotmobile.retrofit.response.MenaraApiResponse;
import com.simp.hostpotmobile.retrofit.response.PolygonBlokResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("gps/polygonBlok")
    Call<PolygonBlokResponse> getPolygonBlok(@Query("memberCode") String memberCode);

    @GET("hotspot/getDataMenaraApi")
    Call<MenaraApiResponse> getDataMenaraApi(@Query("curpage") int curpage, @Query("pagesize") int pagesize);

    @GET("hotspot/getDataHotspotByDate")
    Call<HotspotResponse> getDataHotspotByDate(@Query("startDate") String startDate, @Query("endDate") String endDate,
                                               @Query("curpage") int curpage, @Query("pagesize") int pagesize);

}
