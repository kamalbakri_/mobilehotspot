package com.simp.hostpotmobile.workmanager;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.simp.hostpotmobile.database.entities.HotspotEntities;
import com.simp.hostpotmobile.database.repository.DbHotspotRepository;
import com.simp.hostpotmobile.retrofit.ApiClient;
import com.simp.hostpotmobile.retrofit.ApiInterface;
import com.simp.hostpotmobile.retrofit.response.Hotspot;
import com.simp.hostpotmobile.retrofit.response.HotspotResponse;
import com.simp.hostpotmobile.utils.Constant;


import retrofit2.Call;
import retrofit2.Response;

public class HotspotWorker extends Worker {

    ApiInterface apiInterface;
    DbHotspotRepository dbHotspotRepository;

    public HotspotWorker(@NonNull Context appContext, @NonNull WorkerParameters workerParams) {
        super(appContext, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        try {
            apiInterface = ApiClient.getClient(Constant.BASE_URL).create(ApiInterface.class);
            dbHotspotRepository = new DbHotspotRepository(getApplicationContext());
            Response<HotspotResponse> response = getHotspotData("2020-01-01", "2020-04-07", 0, 1000).execute();
            if (response.code() == 200) {
                HotspotResponse hotspotResponse = fetchHotspotData(response);
                dbHotspotRepository.deleteAllHotspot();
                for (Hotspot hotspot : hotspotResponse.getHotspot()) {
                    HotspotEntities hotspotEntities = new HotspotEntities();
                    hotspotEntities.setObjectid(hotspot.getObjectid());
                    hotspotEntities.setNo(hotspot.getNo());
                    hotspotEntities.setLongitude(hotspot.getLongitude());
                    hotspotEntities.setLatitude(hotspot.getLatitude());
                    hotspotEntities.setDatetime(hotspot.getDatetime());
                    hotspotEntities.setSatellite(hotspot.getSatellite());
                    hotspotEntities.setAddress(hotspot.getAddress());
                    hotspotEntities.setArea(hotspot.getArea());
                    hotspotEntities.setDistanceKm(hotspot.getDistanceKm());
                    hotspotEntities.setSudut(hotspot.getSudut());
                    hotspotEntities.setBusArea(hotspot.getBusArea());
                    hotspotEntities.setDirection(hotspot.getDirection());
                    hotspotEntities.setRing(hotspot.getRing());
                    hotspotEntities.setUploadBy(hotspot.getUploadBy());
                    hotspotEntities.setUploadDate(hotspot.getUploadDate());
                    hotspotEntities.setKonfirmasi(hotspot.getKonfirmasi());
                    hotspotEntities.setKonfirmDate(hotspot.getKonfirmDate());
                    hotspotEntities.setBlock(hotspot.getBlock());
                    hotspotEntities.setKet(hotspot.getKet());
                    hotspotEntities.setKodeArea(hotspot.getKodeArea());
                    hotspotEntities.setPt(hotspot.getPt());
                    hotspotEntities.setGrup(hotspot.getGrup());
                    hotspotEntities.setHotspotID(hotspot.getHotspotID());
                    dbHotspotRepository.insertHotspot(hotspotEntities);
                }
                Result.success();
            } else {
                Result.failure();
            }
        } catch (Exception e) {
            Result.failure();
        }
        return Result.success();
    }

    public Call<HotspotResponse> getHotspotData(String startDate, String endDate, int curpage, int pagesize) {
        return apiInterface.getDataHotspotByDate(startDate, endDate, curpage, pagesize);
    }

    public HotspotResponse fetchHotspotData(Response<HotspotResponse> response) {
        return response.body();
    }
}
