package com.simp.hostpotmobile.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.simp.hostpotmobile.database.entities.HotspotEntities;
import com.simp.hostpotmobile.retrofit.response.Hotspot;

import java.util.List;

@Dao
public interface HotspotDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(HotspotEntities hotspotEntities);

    @Update
    void update(HotspotEntities hotspotEntities);

    @Query("Select * From table_hotspot")
    LiveData<List<HotspotEntities>> selectAll();

    @Query("Delete From table_hotspot")
    void deleteAll();
}
