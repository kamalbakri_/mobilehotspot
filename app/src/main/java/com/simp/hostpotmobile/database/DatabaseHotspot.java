package com.simp.hostpotmobile.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.simp.hostpotmobile.database.dao.HotspotDao;
import com.simp.hostpotmobile.database.dao.MenaraApiDao;
import com.simp.hostpotmobile.database.dao.PolygonBlokDao;
import com.simp.hostpotmobile.database.entities.HotspotEntities;
import com.simp.hostpotmobile.database.entities.MenaraApiEntities;
import com.simp.hostpotmobile.database.entities.PolygonBlokEntities;
import com.simp.hostpotmobile.utils.Constant;

@Database(entities = {HotspotEntities.class, MenaraApiEntities.class, PolygonBlokEntities.class},
        version = Constant.DB_VERSION, exportSchema = false)
public abstract class DatabaseHotspot extends RoomDatabase {

    public abstract HotspotDao hotspotDao();

    public abstract MenaraApiDao menaraApiDao();

    public abstract PolygonBlokDao polygonBlokDao();

}
