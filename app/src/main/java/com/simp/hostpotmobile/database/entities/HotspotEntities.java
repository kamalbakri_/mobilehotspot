package com.simp.hostpotmobile.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "table_hotspot")
public class HotspotEntities {

    @PrimaryKey
    @ColumnInfo(name ="objectid")
    private Integer objectid;
    @ColumnInfo(name ="no")
    private Integer no;
    @ColumnInfo(name ="longitude")
    private Double longitude;
    @ColumnInfo(name ="latitude")
    private Double latitude;
    @ColumnInfo(name ="datetime")
    private String datetime;
    @ColumnInfo(name ="satellite")
    private String satellite;
    @ColumnInfo(name ="address")
    private String address;
    @ColumnInfo(name ="area")
    private String area;
    @ColumnInfo(name ="distance_km")
    private Double distanceKm;
    @ColumnInfo(name ="sudut")
    private Double sudut;
    @ColumnInfo(name ="busArea")
    private String busArea;
    @ColumnInfo(name ="direction")
    private String direction;
    @ColumnInfo(name ="ring")
    private String ring;
    @ColumnInfo(name ="uploadBy")
    private String uploadBy;
    @ColumnInfo(name ="uploadDate")
    private String uploadDate;
    @ColumnInfo(name ="konfirmasi")
    private String konfirmasi;
    @ColumnInfo(name ="konfirmDate")
    private String konfirmDate;
    @ColumnInfo(name ="block")
    private String block;
    @ColumnInfo(name ="ket")
    private String ket;
    @ColumnInfo(name ="kodeArea")
    private String kodeArea;
    @ColumnInfo(name ="pt")
    private String pt;
    @ColumnInfo(name ="grup")
    private String grup;
    @ColumnInfo(name ="hotspotID")
    private String hotspotID;

    public Integer getObjectid() {
        return objectid;
    }

    public void setObjectid(Integer objectid) {
        this.objectid = objectid;
    }

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getSatellite() {
        return satellite;
    }

    public void setSatellite(String satellite) {
        this.satellite = satellite;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Double getDistanceKm() {
        return distanceKm;
    }

    public void setDistanceKm(Double distanceKm) {
        this.distanceKm = distanceKm;
    }

    public Double getSudut() {
        return sudut;
    }

    public void setSudut(Double sudut) {
        this.sudut = sudut;
    }

    public String getBusArea() {
        return busArea;
    }

    public void setBusArea(String busArea) {
        this.busArea = busArea;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getRing() {
        return ring;
    }

    public void setRing(String ring) {
        this.ring = ring;
    }

    public String getUploadBy() {
        return uploadBy;
    }

    public void setUploadBy(String uploadBy) {
        this.uploadBy = uploadBy;
    }

    public String getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getKonfirmasi() {
        return konfirmasi;
    }

    public void setKonfirmasi(String konfirmasi) {
        this.konfirmasi = konfirmasi;
    }

    public String getKonfirmDate() {
        return konfirmDate;
    }

    public void setKonfirmDate(String konfirmDate) {
        this.konfirmDate = konfirmDate;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }

    public String getKodeArea() {
        return kodeArea;
    }

    public void setKodeArea(String kodeArea) {
        this.kodeArea = kodeArea;
    }

    public String getPt() {
        return pt;
    }

    public void setPt(String pt) {
        this.pt = pt;
    }

    public String getGrup() {
        return grup;
    }

    public void setGrup(String grup) {
        this.grup = grup;
    }

    public String getHotspotID() {
        return hotspotID;
    }

    public void setHotspotID(String hotspotID) {
        this.hotspotID = hotspotID;
    }
}
