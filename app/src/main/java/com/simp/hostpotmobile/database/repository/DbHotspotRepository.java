package com.simp.hostpotmobile.database.repository;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.room.Room;

import com.simp.hostpotmobile.database.DatabaseHotspot;
import com.simp.hostpotmobile.database.entities.HotspotEntities;
import com.simp.hostpotmobile.database.entities.MenaraApiEntities;
import com.simp.hostpotmobile.database.entities.PolygonBlokEntities;
import com.simp.hostpotmobile.utils.Constant;

import java.util.List;

public class DbHotspotRepository {
    private DatabaseHotspot databaseTracking;

    public DbHotspotRepository(Context context) {
        databaseTracking = Room.databaseBuilder(context, DatabaseHotspot.class, Constant.DB_NAME)
                .build();
    }

    public void insertHotspot(HotspotEntities hotspotEntities) {
        databaseTracking.hotspotDao().insert(hotspotEntities);
    }

    public void updateHotspot(HotspotEntities hotspotEntities) {
        databaseTracking.hotspotDao().update(hotspotEntities);
    }

    public LiveData<List<HotspotEntities>> selectAllHotspot() {
        return databaseTracking.hotspotDao().selectAll();
    }

    public void deleteAllHotspot() {
        databaseTracking.hotspotDao().deleteAll();
    }

    public void insertMenaraApi(MenaraApiEntities menaraApiEntities) {
        databaseTracking.menaraApiDao().insert(menaraApiEntities);
    }

    public void updateMenaraApi(MenaraApiEntities menaraApiEntities) {
        databaseTracking.menaraApiDao().update(menaraApiEntities);
    }

    public LiveData<List<MenaraApiEntities>> selectAllMenaraApi() {
        return databaseTracking.menaraApiDao().selectAll();
    }

    public void deleteAllMenaraApi() {
        databaseTracking.menaraApiDao().deleteAll();
    }

    public void insertPolygonBlok(PolygonBlokEntities polygonBlokEntities) {
        databaseTracking.polygonBlokDao().insert(polygonBlokEntities);
    }

    public void updatePolygonBlok(PolygonBlokEntities polygonBlokEntities) {
        databaseTracking.polygonBlokDao().update(polygonBlokEntities);
    }

    public LiveData<List<PolygonBlokEntities>> selectMenaraApiByKodeArea(String kodeArea) {
        return databaseTracking.polygonBlokDao().selectByKodeArea(kodeArea);
    }

    public void deleteAllPolygonBlok() {
        databaseTracking.polygonBlokDao().deleteAll();
    }

    public LiveData<Integer>  countAllMenaraApi() {
        return databaseTracking.menaraApiDao().countAll();
    }

}
