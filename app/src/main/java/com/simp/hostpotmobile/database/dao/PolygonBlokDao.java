package com.simp.hostpotmobile.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.simp.hostpotmobile.database.entities.PolygonBlokEntities;
import com.simp.hostpotmobile.retrofit.response.PolygonBlok;

import java.util.List;

@Dao
public interface PolygonBlokDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(PolygonBlokEntities polygonBlokEntities);

    @Update
    void update(PolygonBlokEntities polygonBlokEntities);

    @Query("Select * from table_polygon_blok where kodearea = :kodearea")
    LiveData<List<PolygonBlokEntities>> selectByKodeArea(String kodearea);

    @Query("Delete From table_polygon_blok")
    void deleteAll();
}
