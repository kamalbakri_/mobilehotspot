package com.simp.hostpotmobile.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "table_menaraapi")
public class MenaraApiEntities {

    @PrimaryKey
    @ColumnInfo(name = "objectid")
    private Integer objectid;
    @ColumnInfo(name = "id")
    private Integer id;
    @ColumnInfo(name = "area")
    private String area;
    @ColumnInfo(name = "busArea")
    private String busArea;
    @ColumnInfo(name = "no")
    private Integer no;
    @ColumnInfo(name = "informasi")
    private String informasi;
    @ColumnInfo(name = "divisi")
    private String divisi;
    @ColumnInfo(name = "kode_Blok")
    private String kodeBlok;
    @ColumnInfo(name = "the_Geom")
    private String theGeom;
    @ColumnInfo(name = "longitude")
    private Double longitude;
    @ColumnInfo(name = "latitude")
    private Double latitude;

    public Integer getObjectid() {
        return objectid;
    }

    public void setObjectid(Integer objectid) {
        this.objectid = objectid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getBusArea() {
        return busArea;
    }

    public void setBusArea(String busArea) {
        this.busArea = busArea;
    }

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public String getInformasi() {
        return informasi;
    }

    public void setInformasi(String informasi) {
        this.informasi = informasi;
    }

    public String getDivisi() {
        return divisi;
    }

    public void setDivisi(String divisi) {
        this.divisi = divisi;
    }

    public String getKodeBlok() {
        return kodeBlok;
    }

    public void setKodeBlok(String kodeBlok) {
        this.kodeBlok = kodeBlok;
    }

    public String getTheGeom() {
        return theGeom;
    }

    public void setTheGeom(String theGeom) {
        this.theGeom = theGeom;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
}
