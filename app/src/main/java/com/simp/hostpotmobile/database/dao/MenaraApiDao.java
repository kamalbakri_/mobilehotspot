package com.simp.hostpotmobile.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.simp.hostpotmobile.database.entities.MenaraApiEntities;

import java.util.List;

@Dao
public interface MenaraApiDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(MenaraApiEntities menaraApiEntities);

    @Update
    void update(MenaraApiEntities menaraApiEntities);

    @Query("Select * From table_menaraapi")
    LiveData<List<MenaraApiEntities>> selectAll();

    @Query("Delete From table_menaraapi")
    void deleteAll();

    @Query("Select Count(*) From table_menaraapi")
    LiveData<Integer> countAll();
}
