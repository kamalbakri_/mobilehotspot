package com.simp.hostpotmobile.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "table_polygon_blok")
public class PolygonBlokEntities {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;
    private Integer objectiD1;
    @ColumnInfo(name = "bloK_ID")
    private String bloKID;
    @ColumnInfo(name = "kodE_KEBUN")
    private String kodE_KEBUN;
    @ColumnInfo(name = "kodE_BLOK")
    private String kodEBLOK;
    @ColumnInfo(name = "busarea")
    private String busarea;
    @ColumnInfo(name = "kodearea")
    private String kodearea;
    @ColumnInfo(name = "kodedivisi")
    private String kodedivisi;
    @ColumnInfo(name = "division")
    private String division;
    @ColumnInfo(name = "the_geom")
    private String theGeom;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getObjectiD1() {
        return objectiD1;
    }

    public void setObjectiD1(Integer objectiD1) {
        this.objectiD1 = objectiD1;
    }

    public String getBloKID() {
        return bloKID;
    }

    public void setBloKID(String bloKID) {
        this.bloKID = bloKID;
    }

    public String getKodE_KEBUN() {
        return kodE_KEBUN;
    }

    public void setKodE_KEBUN(String kodE_KEBUN) {
        this.kodE_KEBUN = kodE_KEBUN;
    }

    public String getKodEBLOK() {
        return kodEBLOK;
    }

    public void setKodEBLOK(String kodEBLOK) {
        this.kodEBLOK = kodEBLOK;
    }

    public String getBusarea() {
        return busarea;
    }

    public void setBusarea(String busarea) {
        this.busarea = busarea;
    }

    public String getKodearea() {
        return kodearea;
    }

    public void setKodearea(String kodearea) {
        this.kodearea = kodearea;
    }

    public String getKodedivisi() {
        return kodedivisi;
    }

    public void setKodedivisi(String kodedivisi) {
        this.kodedivisi = kodedivisi;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getTheGeom() {
        return theGeom;
    }

    public void setTheGeom(String theGeom) {
        this.theGeom = theGeom;
    }
}
