package com.simp.hostpotmobile.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.simp.hostpotmobile.R;
import com.simp.hostpotmobile.activity.MapHotspotActivity;
import com.simp.hostpotmobile.activity.SummaryHotspotActivity;
import com.simp.hostpotmobile.database.entities.HotspotEntities;
import com.simp.hostpotmobile.database.entities.MenaraApiEntities;
import com.simp.hostpotmobile.database.repository.DbHotspotRepository;
import com.simp.hostpotmobile.retrofit.response.Hotspot;
import com.simp.hostpotmobile.utils.Permissions;
import com.simp.hostpotmobile.utils.ViewAnimation;
import com.simp.hostpotmobile.workmanager.HotspotWorker;

import java.util.HashMap;
import java.util.List;

public class HomeFragment extends Fragment implements View.OnClickListener
        , OnMapReadyCallback
        , GoogleMap.OnCameraIdleListener
        , GoogleMap.OnMarkerClickListener {

    View view;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public float current_map_zoom_level = 7;

    boolean isRotate = false;
    String mapType = "default";

    FloatingActionButton fabMenu, fabTable, fabFilter, fabBasemap, fabLayer, fabLegenda,
            fabZoomIn, fabZoomOut, fabCurrentPosition;
    GoogleMap gMap;

    private DbHotspotRepository dbHotspotRepository;
    private HashMap<Integer, Marker> menaraApiHashMap = new HashMap<>();
    private HashMap<Integer, Marker> hotspotHashMap = new HashMap<>();

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);

        dbHotspotRepository = new DbHotspotRepository(getActivity());
        initView();
        startWorkManager();

        // Get the SupportMapFragment and request notification when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        return view;
    }

    public void initView() {
        fabMenu = view.findViewById(R.id.fab_menu);
        fabTable = view.findViewById(R.id.fab_table);
        fabFilter = view.findViewById(R.id.fab_filter);
        fabBasemap = view.findViewById(R.id.fab_basemap);
        fabLayer = view.findViewById(R.id.fab_layer);
        fabLegenda = view.findViewById(R.id.fab_legenda);
        fabZoomIn = view.findViewById(R.id.fab_zoomin);
        fabZoomOut = view.findViewById(R.id.fab_zoomout);
        fabCurrentPosition = view.findViewById(R.id.fab_current);


        fabMenu.setOnClickListener(this);
        fabTable.setOnClickListener(this);
        fabFilter.setOnClickListener(this);
        fabBasemap.setOnClickListener(this);
        fabLayer.setOnClickListener(this);
        fabLegenda.setOnClickListener(this);
        fabCurrentPosition.setOnClickListener(this);
        fabZoomIn.setOnClickListener(this);
        fabZoomOut.setOnClickListener(this);

        ViewAnimation.init(fabTable);
        ViewAnimation.init(fabFilter);
        ViewAnimation.init(fabBasemap);
        ViewAnimation.init(fabLayer);
        ViewAnimation.init(fabLegenda);
    }


    public void hideStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getActivity().getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_menu:
                showMenu(view);
                break;
            case R.id.fab_legenda:
                showLegend();
                break;
            case R.id.fab_layer:
                break;
            case R.id.fab_filter:
                showFilter();
                break;
            case R.id.fab_basemap:
                showBasemap();
                break;
            case R.id.fab_table:
                Intent intent = new Intent(getActivity(), SummaryHotspotActivity.class);
                startActivity(intent);
                break;
            case R.id.fab_current:
                if (!Permissions.checkFineLocation(getActivity())) {
                    Permissions.requestFineLocation(getActivity(), MY_PERMISSIONS_REQUEST_LOCATION);
                } else {
                    getCurrentPosition();
                }
                break;
            case R.id.fab_zoomin:
                gMap.animateCamera(CameraUpdateFactory.zoomIn());
                break;
            case R.id.fab_zoomout:
                gMap.animateCamera(CameraUpdateFactory.zoomOut());
                break;

            default:
                break;
        }
    }

    public void showMenu(View view) {
        isRotate = ViewAnimation.rotateFab(view, !isRotate, getActivity());
        if (isRotate) {
            ViewAnimation.showIn(fabTable);
            ViewAnimation.showIn(fabFilter);
            ViewAnimation.showIn(fabBasemap);
            ViewAnimation.showIn(fabLayer);
            ViewAnimation.showIn(fabLegenda);
        } else {
            ViewAnimation.showOut(fabTable);
            ViewAnimation.showOut(fabFilter);
            ViewAnimation.showOut(fabBasemap);
            ViewAnimation.showOut(fabLayer);
            ViewAnimation.showOut(fabLegenda);
        }
    }

    public void showLegend() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        View dialoglayout = getLayoutInflater().inflate(R.layout.dialog_legend, null);
        dialogBuilder.setView(dialoglayout);
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
    }

    public void showBasemap() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        View dialoglayout = getLayoutInflater().inflate(R.layout.dialog_basemap, null);
        dialogBuilder.setView(dialoglayout);

        RadioGroup rgBasemap = dialoglayout.findViewById(R.id.rg_basemap);

        if (mapType.equals("default")) {
            rgBasemap.check(R.id.rb_map_default);
        } else if (mapType.equals("satellite")) {
            rgBasemap.check(R.id.rb_map_satellite);
        }

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();

        rgBasemap.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.rb_map_default:
                        if (!mapType.equals("default")) {
                            mapType = "default";
                            gMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                        }
                        alertDialog.dismiss();
                        break;
                    case R.id.rb_map_satellite:
                        if (!mapType.equals("satellite")) {
                            mapType = "satellite";
                            gMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                        }
                        alertDialog.dismiss();
                        break;
                    default:
                        break;
                }
            }
        });
    }


    public void showFilter() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        View dialoglayout = getLayoutInflater().inflate(R.layout.dialog_filter, null);
        dialogBuilder.setView(dialoglayout);
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        gMap.setOnCameraIdleListener(this);
        gMap.setOnMarkerClickListener(this);
        LatLng indonesia = new LatLng(-6.2082572, 106.8226707);
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(indonesia, current_map_zoom_level));
    }

    public void getCurrentPosition() {
        // GET CURRENT LOCATION
        FusedLocationProviderClient mFusedLocation = LocationServices.getFusedLocationProviderClient(getActivity());
        mFusedLocation.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    Marker currMarker = gMap.addMarker(new MarkerOptions().position(
                            new LatLng(location.getLatitude(), location.getLongitude())).
                            flat(true));
                    currMarker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_current_position));
                    gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 17));
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getCurrentPosition();
                }
            }
        }
    }

    @Override
    public void onCameraIdle() {
        CameraPosition cameraPosition = gMap.getCameraPosition();
        current_map_zoom_level = cameraPosition.zoom;
        if (current_map_zoom_level >= 7) {
            if (menaraApiHashMap.isEmpty())
                getMenaraApi();
            if (hotspotHashMap.isEmpty())
                getDataHotspot();
        } else {
            for (Marker m : menaraApiHashMap.values()) {
                m.remove();
            }
            menaraApiHashMap.clear();
            for (Marker m : hotspotHashMap.values()) {
                m.remove();
            }
            hotspotHashMap.clear();
        }
    }

    public void getMenaraApi() {
        dbHotspotRepository.selectAllMenaraApi().observe(this, new Observer<List<MenaraApiEntities>>() {
            @Override
            public void onChanged(List<MenaraApiEntities> menaraApiEntities) {
                displayMenaraApi(menaraApiEntities);
            }
        });
    }

    public void displayMenaraApi(List<MenaraApiEntities> menaraApiEntitiesList) {
        for (MenaraApiEntities menaraApiEntities : menaraApiEntitiesList) {
            if (menaraApiEntities.getLatitude() != null && menaraApiEntities.getLongitude() != null) {
                Marker markerFireTower = gMap.addMarker(new MarkerOptions().position(
                        new LatLng(menaraApiEntities.getLatitude(), menaraApiEntities.getLongitude())).
                        flat(true).title(menaraApiEntities.getBusArea()));
                markerFireTower.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_firetower));
                menaraApiHashMap.put(menaraApiEntities.getObjectid(), markerFireTower);
            }
        }
    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 15));
        marker.showInfoWindow();
        return false;
    }

    public void getDataHotspot() {
        dbHotspotRepository.selectAllHotspot().observe(this, new Observer<List<HotspotEntities>>() {
            @Override
            public void onChanged(List<HotspotEntities> hotspotEntities) {
                displayHotspot(hotspotEntities);
            }
        });
    }

    public void displayHotspot(List<HotspotEntities> hotspotEntitiesList) {
        for (HotspotEntities hotspotEntities : hotspotEntitiesList) {
            if (hotspotEntities.getLatitude() != null && hotspotEntities.getLongitude() != null) {
                Marker markerFireTower = gMap.addMarker(new MarkerOptions().position(
                        new LatLng(hotspotEntities.getLatitude(), hotspotEntities.getLongitude())).
                        flat(true).title(hotspotEntities.getSatellite()));
                if (hotspotEntities.getRing().equals("1")) {
                    if (hotspotEntities.getSatellite().toUpperCase().contains("NOAA") || hotspotEntities.getSatellite().equalsIgnoreCase("SNPP")) {
                        markerFireTower.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_noaa_dalam));
                    } else {
                        markerFireTower.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_nasa_dalam));
                    }
                } else {
                    if (hotspotEntities.getSatellite().toUpperCase().contains("NOAA") || hotspotEntities.getSatellite().equalsIgnoreCase("SNPP")) {
                        markerFireTower.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_noaa_luar));
                    } else {
                        markerFireTower.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_nasa_luar));
                    }
                }
                menaraApiHashMap.put(hotspotEntities.getObjectid(), markerFireTower);
            }
        }
    }

    public void startWorkManager() {
        WorkManager workManager = WorkManager.getInstance(getActivity());
        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        OneTimeWorkRequest work = new OneTimeWorkRequest.Builder(HotspotWorker.class).setConstraints(constraints)
                .build();
        workManager.beginWith(work).enqueue();
    }
}
